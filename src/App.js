import React from 'react';
import Chat from './chat/index'


function App() {
  return (
    <div className="App">
        <Chat />
    </div>
  );
}

export default App;
