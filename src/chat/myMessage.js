import React from 'react'
import '../styles/Message.css'

export default class MyMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isShown: false }
        this.onDelete = this.onDelete.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onMouseEnter = this.onMouseEnter.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);
    }

    onDelete() {
        this.props.deleteMessage(this.props.data.id);
    }

    onEdit() {
        this.props.showPage(this.props.data.id);
    }

    onChange(e) {
        this.setState({ value: e.target.value });
    }

    onSave() {
        this.props.editMessage(this.props.data, this.state.value);
        this.setState({ isEditing: false });
    }

    getEditButton() {
        return this.state.isShown ? 
        <i className='fa fa-cog' onClick={this.onEdit}></i> : null;
    } 

    onMouseEnter() {
        this.setState({ isShown: true });
    }

    onMouseLeave() {
        this.setState({ isShown: false });
    }

    render() {
        const data = this.props.data;
        return (
            <div className="my-message-container" onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
                <div className='message-text'>{data.message}</div>
                <div className='message-date'>{data.created_at}</div>
                <i className='fa fa-heart'></i>
                <i className='fa fa-times' onClick={this.onDelete}></i>
                {this.getEditButton()}
            </div>
        );
    }
}