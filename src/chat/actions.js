import { FETCH_DATA_SUCCESS, FETCH_DATA_FAILURE, ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE } from './actionTypes'

export const fetchDataSuccess = data => ({
    type: FETCH_DATA_SUCCESS,
    payload: data
});

export const fetchDataFailure = error => ({
    type: FETCH_DATA_FAILURE,
    payload: error
});

export const addMessage = message => ({
    type: ADD_MESSAGE,
    payload: message
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: id
});

export const editMessage = (id, message) => ({
    type: EDIT_MESSAGE,
    payload: message,
    messageId: id
})