import React from 'react';
import { connect } from "react-redux";
import * as actions from './actions';
import { showPage } from '../editPage/actions'
import Header from './header'
import MessageList from './messageList';
import '../styles/chat.css'
import MessageInput from './messageInput';
import EditPage from '../editPage/index'


class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.showPage = this.showPage.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        return fetch('https://api.myjson.com/bins/1hiqin', { method: 'GET' })
          .then(this.handleErrors)
          .then(res => res.json())
          .then(data => {
            this.props.fetchDataSuccess(data);
          })
          .catch(error => this.props.fetchDataFailure(error));
      }
      
    handleErrors(response) {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
    }

    getUsersAmount() {
        const data = this.props.data;
        const usersList = data.map(message => message.user);
        return new Set(usersList).size;
    }

    sendMessage(message) {
        this.props.addMessage(message);
    }

    deleteMessage(id) {
        this.props.deleteMessage(id);
    }

    showPage(id) {
        this.props.showPage(id);
    }

    onKeyDown(e) {
        if (e.keyCode === 38) {
            const myMessages = this.props.data.filter(message => message.user === 'me');
            if (myMessages.length) {
                const id = myMessages[myMessages.length - 1].id;
                this.props.showPage(id);
            }
        }
    }

    render() {   
        const { error, loading, data } = this.props;

        if (loading) return (<div className='spinner'></div>);

        if (error) {
            return <div>Error! {error.message}</div>;
        }
        
        console.log(data);
        let messagesAmount = data.length;
        return (
            <div onKeyDown={this.onKeyDown} tabIndex="0">
                <Header 
                    usersAmount={this.getUsersAmount()}
                    messagesAmount={messagesAmount}
                    lastMessage={data[messagesAmount - 1].created_at}
                />
                <MessageList 
                    data={data}
                    deleteMessage={this.deleteMessage}
                    showPage={this.showPage}
                />
                <MessageInput 
                    sendMessage={this.sendMessage}
                />
                <EditPage />
            </div>
        );
        
    }

}

const mapStateToProps = state => ({
    data: state.chat.data,
    loading: state.chat.loading,
    error: state.chat.error
});

const mapDispatchToProps = {
    ...actions, showPage
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
