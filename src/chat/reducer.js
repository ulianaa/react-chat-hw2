import { FETCH_DATA_SUCCESS, FETCH_DATA_FAILURE, ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE } from './actionTypes'

const initialState = {
    data: [],
    loading: true,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_DATA_SUCCESS: {
            return {
                error: null,
                loading: false,
                data: action.payload
            };
        }

        case FETCH_DATA_FAILURE: {
            return {
                loading: false,
                error: action.payload.error,
                data: []
            }
        }

        case ADD_MESSAGE: {
            return {
                ...state,
                data: [...state.data, action.payload ]
            }
        }

        case DELETE_MESSAGE: {
            const newData = state.data.filter(message => message.id !== action.payload);
            return {
                ...state,
                data: newData
            }
        }

        case EDIT_MESSAGE: {
            const updatedData = state.data.map(data => {
                if (data.id === action.messageId) {
                    return {
                        ...data,
                        message: action.payload
                    };
                } else {
                    return data;
                }
            });

            return {...state, data: updatedData};
        }

        default: 
            return state; 
    }
}