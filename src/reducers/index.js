import { combineReducers } from "redux";
import chat from '../chat/reducer'
import editPage from '../editPage/reducer'

const rootReducer = combineReducers({
    chat, editPage
});

export default rootReducer;