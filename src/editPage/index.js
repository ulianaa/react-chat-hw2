import React from 'react'
import { connect } from 'react-redux'
import { editMessage } from '../chat/actions'
import * as actions from './actions';

class EditPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.onChange = this.onChange.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onEdit = this.onEdit.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const id = nextProps.messageId;
        if (this.props.messageId !== id && id !== '') {
        const data = this.props.data.find(data => data.id === nextProps.messageId);
        this.setState({ value: data.message });
        }
    }

    onChange(e) {
        this.setState({ value: e.target.value });
    }

    onCancel() {
        this.props.hidePage();
        this.setState({ value: '' });
    }

    onEdit() {
        this.props.hidePage();
        this.props.editMessage(this.props.messageId, this.state.value);
    }

    getEditPageContent() {
        const message = this.state.value;

        return (
            <div className="editPage">
                <form>
                    <p>Edit your message</p>
                    <textarea value={message} onChange={this.onChange}></textarea>
                    <button type='button' onClick={this.onCancel}>cancel</button>
                    <button type='button' onClick={this.onEdit}>save</button>
                </form>
            </div>
        );
    }

    render() {
        const isShown = this.props.isShown;
        return isShown ? this.getEditPageContent() : null;
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.chat.data,
        isShown: state.editPage.isShown,
        messageId: state.editPage.messageId
    }
};

const mapDispatchToProps = {
    ...actions, editMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPage);
