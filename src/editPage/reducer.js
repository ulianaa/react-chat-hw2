import { HIDE_PAGE, SHOW_PAGE } from './actionTypes'

const initialState = {
    isShown: false,
    messageId: ''
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SHOW_PAGE: {
            return {
                isShown: true,
                messageId: action.payload
            }
        }

        case HIDE_PAGE: {
            return {
                isShown: false,
                messageId: ''
            }
        }

        default: 
            return state;
    }
}

